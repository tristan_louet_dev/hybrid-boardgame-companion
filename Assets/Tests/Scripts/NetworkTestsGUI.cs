﻿using Assets.commons.Scripts.Entity;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Tests.Scripts
{
    public class NetworkTestsGUI : MonoBehaviour {
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                var order = new PlayerOrder();
                order.Push(Character.Amy);
                order.Push(Character.Dominic);
                order.Push(Character.Jake);
                ClientNetworkManager.Instance.NetworkAPI.ActionPlayerOrderChosen(order);
            }
        }
    }
}
