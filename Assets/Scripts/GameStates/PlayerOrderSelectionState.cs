﻿using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.GameStates;
using Assets.commons.Scripts.Utils;
using Assets.Scripts.Managers;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.GameStates {
    public class PlayerOrderSelectionState : StateBase
    {
        private GameObject _playerOrderPanel;
        private UIButton _jakeButton;
        private UIButton _amyButton;
        private UIButton _domButton;

        private PlayerOrder _order;

        public PlayerOrder Order
        {
            get
            {
                return new PlayerOrder(_order);
            }
        }

        public Event<PlayerOrder> onOrderReady;
        
        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="playerOrderPanel">Panel for player order.</param>
        public PlayerOrderSelectionState(GameObject playerOrderPanel)
        {
            _playerOrderPanel = playerOrderPanel;
            _jakeButton = playerOrderPanel.transform.FindChild("Jake Button").GetComponent<UIButton>();
            _amyButton = playerOrderPanel.transform.FindChild("Amy Button").GetComponent<UIButton>();
            _domButton = playerOrderPanel.transform.FindChild("Dom Button").GetComponent<UIButton>();
        }

        public override void OnActivated()
        {
            base.OnActivated();
            _order = new PlayerOrder();
            onOrderReady = new Event<PlayerOrder>();
            _jakeButton.isEnabled = true;
            _amyButton.isEnabled = true;
            _domButton.isEnabled = true;
            _jakeButton.onClick.Add(new EventDelegate(JakeSelected));
            _amyButton.onClick.Add(new EventDelegate(AmySelected));
            _domButton.onClick.Add(new EventDelegate(DomSelected));
            _playerOrderPanel.SetActive(true);
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();
            _playerOrderPanel.SetActive(false);
            _jakeButton.onClick.Clear();
            _amyButton.onClick.Clear();
            _domButton.onClick.Clear();
        }

        public override void OnUpdated()
        {
        }

        private void JakeSelected()
        {
            _jakeButton.isEnabled = false;
            #region Stack consistensy check
#if UNITY_EDITOR
            AssertStackConsistensy(Character.Jake);
#endif
            #endregion
            _order.Push(Character.Jake);
            CheckPlayerOrderReadiness();
        }

        private void AmySelected()
        {
            _amyButton.isEnabled = false;
            #region Stack consistensy check
#if UNITY_EDITOR
            AssertStackConsistensy(Character.Amy);
#endif
            #endregion
            _order.Push(Character.Amy);
            CheckPlayerOrderReadiness();
        }

        private void DomSelected()
        {
            _domButton.isEnabled = false;
            #region Stack consistensy check
#if UNITY_EDITOR
            AssertStackConsistensy(Character.Dominic);
#endif
            #endregion
            _order.Push(Character.Dominic);
            CheckPlayerOrderReadiness();
        }

        private void CheckPlayerOrderReadiness()
        {
            if (_order.Count == 3)
            {
                ClientNetworkManager.Instance.NetworkAPI.ActionPlayerOrderChosen(Order);
            }
        }

        #region Stack consistensy check
#if UNITY_EDITOR
        private void AssertStackConsistensy(Character character)
        {
            if (_order.Contains(character))
            {
                Debug.LogError("Character " + character + " already in player order stack");
            }
        }
#endif
        #endregion
    }
}
