﻿using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.GameStates;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.Managers;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.GameStates {
    public class PlayerTurn : StateBase
    {
        private GameObject _playerTurnPanel;
        private UIButton _confirmButton;
        private UIButton _interactButton;
        private UIButton _cancelButton;

        private bool _canMove;
        private bool _canInteract;

        enum Step2
        {
            Choices,
            Interact,
            Move
        }

        private Step2 _step2;
        private Step2 _Step2
        {
            get { return _step2; }
            set
            {
                _step2 = value;
                _confirmButton.gameObject.SetActive((_Step2 == Step2.Move && _canMove) || (_Step2 == Step2.Interact && _canInteract));
                _interactButton.gameObject.SetActive(_Step2 == Step2.Choices);
                _cancelButton.gameObject.SetActive(_Step2 != Step2.Choices);
            }
        }

        /// <summary>
        /// Initializing constructor.
        /// </summary>
        /// <param name="playerTurnPanel">Panel containing buttons for player turn.</param>
        public PlayerTurn(GameObject playerTurnPanel)
        {
            _playerTurnPanel = playerTurnPanel;
            _confirmButton = playerTurnPanel.transform.FindChild("Confirm button").GetComponent<UIButton>();
            _interactButton = playerTurnPanel.transform.FindChild("Interact button").GetComponent<UIButton>();
            _cancelButton = playerTurnPanel.transform.FindChild("Cancel button").GetComponent<UIButton>();
        }

        public override void OnActivated()
        {
            base.OnActivated();
            _canMove = false;
            _canInteract = false;
            _Step2 = Step2.Choices;
            _confirmButton.onClick.Add(new EventDelegate(ConfirmAction));
            _interactButton.onClick.Add(new EventDelegate(RequestInteractAction));
            _cancelButton.onClick.Add(new EventDelegate(CancelAction));
            _playerTurnPanel.SetActive(true);
            EventManager.onPlayerMoving += OnPlayerMoving;
            EventManager.onPlayerCanMove += OnPlayerCanMove;
            EventManager.onPlayerInteracting += OnPlayerInteracting;
            EventManager.onPlayerCanInteract += OnPlayerCanInteract;
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();
            _playerTurnPanel.SetActive(false);
            _confirmButton.onClick.Clear();
            _interactButton.onClick.Clear();
            _cancelButton.onClick.Clear();
            EventManager.onPlayerMoving -= OnPlayerMoving;
            EventManager.onPlayerCanMove -= OnPlayerCanMove;
            EventManager.onPlayerInteracting -= OnPlayerInteracting;
            EventManager.onPlayerCanInteract -= OnPlayerCanInteract;
        }

        public override void OnUpdated()
        {
        }

        private void OnPlayerMoving(bool value)
        {
            _canMove = false;
            _canInteract = false;
            _Step2 = value ? Step2.Move : Step2.Choices;
        }

        private void OnPlayerCanMove(bool value)
        {
            _canMove = value;
            _Step2 = Step2.Move;
        }

        private void OnPlayerInteracting(bool value)
        {
            _canMove = false;
            _canInteract = false;
            _Step2 = value ? Step2.Interact : Step2.Choices;
        }

        private void OnPlayerCanInteract(bool value)
        {
            _canInteract = value;
            _Step2 = Step2.Interact;
        }

        public void ConfirmAction()
        {
            ClientNetworkManager.Instance.NetworkAPI.ActionConfirm();
        }

        public void RequestInteractAction()
        {
            ClientNetworkManager.Instance.NetworkAPI.ActionInteract();
        }

        public void CancelAction()
        {
            ClientNetworkManager.Instance.NetworkAPI.ActionCancel();
        }
    }
}
