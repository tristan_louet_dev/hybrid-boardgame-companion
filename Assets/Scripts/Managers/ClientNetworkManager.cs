﻿using Assets.commons.Scripts.Network;
using Assets.Scripts.Network;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class ClientNetworkManager : Singleton<ClientNetworkManager> {
        #region Network parameters & properties
        [SerializeField]
        private string _masterServerIP = "localhost";
        [ExposeProperty]
        public string MasterServerIP
        {
            get { return _masterServerIP; }
            private set { _masterServerIP = value; }
        }

        [SerializeField]
        private int _masterServerPort = 23466;
        [ExposeProperty]
        public int MasterServerPort
        {
            get { return _masterServerPort; }
            private set { _masterServerPort = value; }
        }

        [SerializeField]
        private string _facilitatorServerIP = "localhost";
        [ExposeProperty]
        public string FacilitatorServerIP {
            get { return _facilitatorServerIP; }
            private set { _facilitatorServerIP = value; }
        }

        [SerializeField]
        private int _facilitatorServerPort = 50005;
        [ExposeProperty]
        public int FacilitatorServerPort {
            get { return _facilitatorServerPort; }
            private set { _facilitatorServerPort = value; }
        }

        public string mGamePublicName = "default";
        public float mTimeout = 10f;
        private float _timeoutCounter;

        public bool RefreshingHosts { get; private set; }

        public HostData[] Hosts { get; private set; }

        public bool Connected { get; private set; }
        public HostData ConnectedHost { get; private set; }
        #endregion

        #region Utils
        public INetworkAPI NetworkAPI { get; private set; }
        #endregion

        #region Network methods
        public void RefreshHostsList()
        {
            if (RefreshingHosts)
            {
                return;
            }
            MasterServer.RequestHostList(mGamePublicName);
            RefreshingHosts = true;
            EventManager.onHostsRefresh.Fire();
            _timeoutCounter = mTimeout;
            Debug.Log("Refreshing hosts list...");
        }

        public bool Connect(HostData host) {
            var error = UnityEngine.Network.Connect(host);
            Debug.Log(error);
            if (error == NetworkConnectionError.NoError) {
                Connected = true;
                ConnectedHost = host;
                return true;
            }
            Debug.LogError("Error while trying to connect to " + host.gameName + "@" + host.ip[0] + ":" + host.port);
            Debug.LogError(error.ToString());
            return false;
        }

        void OnFailedToConnect()
        {
            Debug.Log("Failed to connect");
        }

        void OnConnectedToServer()
        {
            Debug.Log("Connected to server");
        }
        #endregion
        void OnDisconnectedFromServer(NetworkDisconnection info) {
                Debug.Log("Local server connection disconnected " + info);
        }

        #region MonoBehavior callbacks
        void Start()
        {
            DontDestroyOnLoad(gameObject);
            NetworkAPI = GetComponent<ClientNetworkAPI>();
            MasterServer.ipAddress = MasterServerIP;
            MasterServer.port = MasterServerPort;
            UnityEngine.Network.natFacilitatorIP = FacilitatorServerIP;
            UnityEngine.Network.natFacilitatorPort = FacilitatorServerPort;
            Connected = false;
            ConnectedHost = null;
            RefreshingHosts = false;
            RefreshHostsList();
        }

        void Update()
        {
            if (RefreshingHosts)
            {
                Hosts = MasterServer.PollHostList();
                if (Hosts.Length > 0)
                {
                    RefreshingHosts = false;
                    MasterServer.ClearHostList();
                    EventManager.onHostsListReady.Fire();
                    Debug.Log(Hosts.Length + " hosts found.");
                }
                else
                {
                    _timeoutCounter -= Time.deltaTime;
                    if (_timeoutCounter <= 0f)
                    {
                        Debug.Log("No host found.");
                        RefreshingHosts = false;
                    }
                }
            }
        }
        #endregion
    }
}
