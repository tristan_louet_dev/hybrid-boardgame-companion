﻿using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.GameStates;
using Assets.commons.Scripts.Managers;
using Assets.Scripts.GameStates;
using UnityEngine;

namespace Assets.Scripts.Managers {
    public class CompanionGameManager : GameManager
    {
        /// <summary>
        /// Turn count label.
        /// </summary>
        [SerializeField]
        private UILabel _turnLabel;

        /// <summary>
        /// Character name label.
        /// </summary>
        [SerializeField]
        private UILabel _characterNameLabel;

        /// <summary>
        /// Objective label.
        /// </summary>
        [SerializeField]
        private UILabel _objectiveArea;

        /// <summary>
        /// Panel for player order.
        /// </summary>
        [SerializeField]
        private GameObject _playerOrderPanel;

        /// <summary>
        /// Panel for player turn.
        /// </summary>
        [SerializeField]
        private GameObject _playerTurnPanel;

        public override void Awake()
        {
            base.Awake();
            EventManager.onTurnCountUpdate += OnTurnCountUpdate;
            EventManager.onGameStateUpdate += OnGameStateUpdate;
            EventManager.onObjectiveUpdate += OnObjectiveUpdate;
        }

        protected override void Start()
        {
            base.Start();
            Instance.State = new PlayerOrderSelectionState(_playerOrderPanel);
        }

        private void OnDestroy()
        {
            EventManager.onTurnCountUpdate -= OnTurnCountUpdate;
            EventManager.onGameStateUpdate -= OnGameStateUpdate;
            EventManager.onObjectiveUpdate -= OnObjectiveUpdate;
        }

        /// <summary>
        /// Called when the number of completed turns has been updated.
        /// </summary>
        /// <param name="value">Number of completed turns.</param>
        private void OnTurnCountUpdate(int value)
        {
            TurnCount = value;
            _turnLabel.text = "Turn " + value;
        }

        /// <summary>
        /// Called when the objective has been updated.
        /// </summary>
        /// <param name="value">Objective.</param>
        private void OnObjectiveUpdate(string value)
        {
            _objectiveArea.text = value;
        }

        /// <summary>
        /// Called when the game state has been updated.
        /// </summary>
        /// <param name="state">New state.</param>
        /// <param name="character">New player.</param>
        private void OnGameStateUpdate(GameState state, Character character)
        {
            // Update label.
            _characterNameLabel.text = character == Character.None ? "" : character.ToString();
            // Switch game state.
            switch (state)
            {
                case GameState.PlayerOrderChoice:
                    State = new PlayerOrderSelectionState(_playerOrderPanel);
                    break;
                case GameState.PlayerSpawn:
                    _characterNameLabel.text = "Spawn " + _characterNameLabel.text;
                    State = null;
                    break;
                case GameState.PlayerTurn:
                    State = new PlayerTurn(_playerTurnPanel);
                    break;
                case GameState.AITurn:
                    State = new AITurn();
                    break;
                case GameState.Victory:
                    _turnLabel.text = "Victory";
                    State = null;
                    break;
                default:
                    throw new System.NotImplementedException("The state " + state.ToString() + " is not implemented yet.");
            }
        }
    }
}
