﻿using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.GameStates;
using Assets.commons.Scripts.Utils;

namespace Assets.Scripts.Managers {
    static class EventManager {
        #region Network events

        /// <summary>
        /// Event called when the network manager asks the MasterServer to refresh its hosts list.
        /// </summary>
        public static Event onHostsRefresh = new Event();

        /// <summary>
        /// Event called when the hosts list has been successfully retrieved from the MasterServer.
        /// </summary>
        public static Event onHostsListReady = new Event();

        /// <summary>
        /// Event called when the companion successfully connected to a game server.
        /// </summary>
        public static Event onServerConnectionEstablished = new Event();

        #endregion

        #region Gameplay events
        /// <summary>
        /// Event called when the game server sent the number of completed turns to the companion.
        /// </summary>
        public static Event<int> onTurnCountUpdate = new Event<int>();

        /// <summary>
        /// Event called when the game server sent the objective to the companion.
        /// </summary>
        public static Event<string> onObjectiveUpdate = new Event<string>();

        /// <summary>
        /// Event called when the game server sent a GameState update to the companion.
        /// </summary>
        public static Event<GameState, Character> onGameStateUpdate = new Event<GameState, Character>();

        /// <summary>
        /// Event called when the game server sent an updated life for some character.
        /// </summary>
        public static Event<Character, float> onPlayerLifeUpdated = new Event<Character, float>();

        /// <summary>
        /// Event called when a player is moving. 
        /// </summary>
        public static Event<bool> onPlayerMoving = new Event<bool>();

        /// <summary>
        /// Event called when a player can validate a movement. 
        /// </summary>
        public static Event<bool> onPlayerCanMove = new Event<bool>();

        /// <summary>
        /// Event called when a player is searching for interactions. 
        /// </summary>
        public static Event<bool> onPlayerInteracting = new Event<bool>();

        /// <summary>
        /// Event called when a player can interact with the tile he is facing. 
        /// </summary>
        public static Event<bool> onPlayerCanInteract = new Event<bool>();
        #endregion
    }
}
