﻿using Assets.commons.Scripts.Entity;
using Assets.commons.Scripts.GameStates;
using Assets.commons.Scripts.Network;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Network
{
    public class ClientNetworkAPI : NetworkAPIBase, INetworkAPI {
        #region RPCCall
        [RPC]
        protected override void ServerCallRPC(byte[] values)
        {
            string method;
            var list = Deserialize(values, out method);
            GetType().GetMethod(method).Invoke(this, list);
        }

        [RPC]
        protected override void ClientCallRPC(byte[] values)
        {
            networkView.RPC("ClientCallRPC", RPCMode.Others, values);
        }
        #endregion

        public void UpdateTurnCount(int value)
        {
            EventManager.onTurnCountUpdate.Fire(value);
        }

        public void UpdateObjective(string value)
        {
            EventManager.onObjectiveUpdate.Fire(value);
        }

        public void UpdateGameState(GameState state, Character player = Character.None)
        {
            EventManager.onGameStateUpdate.Fire(state, player);
        }

        public void UpdateLife(Character player, float value)
        {
            EventManager.onPlayerLifeUpdated.Fire(player, value);
        }

        public void PlayerMoving(bool value)
        {
            EventManager.onPlayerMoving.Fire(value);
        }

        public void PlayerCanMove(bool value)
        {
            EventManager.onPlayerCanMove.Fire(value);
        }

        public void PlayerInteracting(bool value)
        {
            EventManager.onPlayerInteracting.Fire(value);
        }

        public void PlayerCanInteract(bool value)
        {
            EventManager.onPlayerCanInteract.Fire(value);
        }

        public void CompanionReady()
        {
            ClientCallRPCSerialize();
        }

        public void ActionPlayerOrderChosen(PlayerOrder order)
        {
            ClientCallRPCSerialize(order);
        }

        public void ActionActiveSkill(int skillId)
        {
            ClientCallRPCSerialize(skillId);
        }

        public void ActionPassiveSkill(int skillId)
        {
            ClientCallRPCSerialize(skillId);
        }

        public void ActionInteract()
        {
            ClientCallRPCSerialize();
        }

        public void ActionConfirm()
        {
            ClientCallRPCSerialize();
        }

        public void ActionCancel()
        {
            ClientCallRPCSerialize();
        }
    }
}
