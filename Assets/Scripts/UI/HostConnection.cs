﻿using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(UILabel))]
    public class HostConnection : MonoBehaviour {
        public HostData Host { get; set; }

        void Start()
        {
            GetComponentInChildren<UILabel>().text = Host.gameName + " - " + (Host.connectedPlayers - 1) + "/" + (Host.playerLimit - 1);
        }

        public void ConnectToHost()
        {
            if (Host == null)
            {
                Debug.LogError("HostData never initialized.");
                return;
            }
            if (ClientNetworkManager.Instance.Connect(Host))
            {
                EventManager.onServerConnectionEstablished.Fire();
            }
            else
            {
                Debug.LogError("Failed to connect.");
            }
        }
    }
}
