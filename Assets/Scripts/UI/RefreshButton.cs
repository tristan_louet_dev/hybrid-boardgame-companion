﻿using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(UIButton))]
    public class RefreshButton : MonoBehaviour
    {
        private UIButton _button;
        void Start()
        {
            _button = GetComponent<UIButton>();
        }

        void Update () {
            _button.isEnabled = !ClientNetworkManager.Instance.RefreshingHosts;
        }
    }
}
