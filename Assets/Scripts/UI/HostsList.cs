﻿using System.Collections.Generic;
using System.Linq;
using Assets.commons.Scripts.Utils;
using Assets.Scripts.Managers;
using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(UIWidget))]
    public class HostsList : MonoBehaviour
    {
        /// <summary>
        /// The NGUI widget that will contain all connection buttons.
        /// </summary>
        private UIWidget _widget;

        [SerializeField]
        private GameObject _buttonPrefab;

        /// <summary>
        /// The connection button prefab.
        /// </summary>
        [ExposeProperty]
        private GameObject ButtonPrefab
        {
            get { return _buttonPrefab; }
            set { _buttonPrefab = value; }
        }

        [SerializeField]
        private float _heightSpacing = 5f;

        /// <summary>
        /// The space between two buttons in the list.
        /// </summary>
        [ExposeProperty]
        public float HeightSpacing
        {
            get { return _heightSpacing; }
            private set { _heightSpacing = value; }
        }

        /// <summary>
        /// The list of instantiated connection buttons.
        /// </summary>
        private List<GameObject> _buttons;

        void Start()
        {
            _buttons = new List<GameObject>();
            _widget = GetComponent<UIWidget>();
            
            EventManager.onHostsListReady += Populate;
            EventManager.onHostsRefresh += () =>
            {
                foreach (var button in _buttons) {
                    Destroy(button);
                }
                _buttons.Clear();
            };

            EventManager.onServerConnectionEstablished += () => AutoFade.LoadLevel("Game", .5f, .5f, Color.black);
        }

        /// <summary>
        /// Populates the GUI list of hosts with buttons that will connect to a launched game.
        /// </summary>
        private void Populate()
        {
            var i = 0;
            foreach (var hostData in ClientNetworkManager.Instance.Hosts)
            {
                var newButton = Instantiate(_buttonPrefab) as GameObject;
                newButton.transform.parent = transform;
                newButton.transform.localScale = new Vector3(1, 1, 1);
                var buttonHeight = newButton.GetComponent<UISprite>().height;
                newButton.transform.localPosition = new Vector3(0, _widget.height / 2f - buttonHeight - i * (buttonHeight + HeightSpacing), 0);
                newButton.GetComponent<HostConnection>().Host = hostData;
                i++;
                _buttons.Add(newButton);
            }
        }
    }
}
